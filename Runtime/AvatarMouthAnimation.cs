﻿using UnityEngine;

namespace CIMMI
{
	public class AvatarMouthAnimation : AvatarBlendshapesAnimation
	{
		[SerializeField, Range(0f, 100f)]
		private float randomValueMinimum = 50;

		[SerializeField, Range(0.01f, 0.5f)]
		private float minAnimationDuration = 0.05f;

		[SerializeField, Range(0.01f, 0.5f)]
		private float maxAnimationDuration = 0.1f;

		private bool isOpening = true;
		private float randomValue, startTime, animationDuration;
		private int mouthIndex;

		protected override void Awake()
		{
			base.Awake();
			blendShapes = new string[] { "CH", "DD", "E", "FF", "RR", "SS", "TH", "aa", "oh", "ou" };
		}

		private void Update()
		{
			if (!IsPlaying) return;

			float t = (Time.time - startTime) / animationDuration / 2f;
			if (t < 1f)
			{
				float value = isOpening ? Mathf.SmoothStep(0, randomValue, t) : Mathf.SmoothStep(randomValue, 0, t);
				avatar.SetBlendShapeWeight(mouthIndex, value);
			}
			else
			{
				if (isOpening)
				{
					startTime = Time.time;
					isOpening = false;
				}
				else
				{
					if (!requestStop)
					{
						NextMouthIndex();
					}
					else
					{
						isOpening = false;
						IsPlaying = false;
						requestStop = false;
					}
				}
			}
		}

		public override void Play()
		{
			base.Play();
			NextMouthIndex();
		}

		private void NextMouthIndex()
		{
			isOpening = true;
			startTime = Time.time;
			randomValue = Random.Range(randomValueMinimum, 100f);

			string blendshape = blendShapes[Random.Range(0, blendShapes.Length)];
			mouthIndex = avatar.sharedMesh.GetBlendShapeIndex(blendshape);

			animationDuration = Random.Range(minAnimationDuration, maxAnimationDuration);
		}

		public void SetRandomValueMinimum(float value) => randomValueMinimum = value;
		public void SetMinAnimationDuration(float value) => minAnimationDuration = value;
		public void SetMaxAnimationDuration(float value) => maxAnimationDuration = value;
	}
}