﻿using UnityEditor;
using UnityEngine;

namespace CIMMI
{
	[CustomEditor(typeof(AvatarBlendshapesAnimation), true)]
	public class AvatarBlendshapesAnimationEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (Application.isPlaying)
			{
				var script = (AvatarBlendshapesAnimation)target;
				bool isPlaying = script.IsPlaying;

				if (GUILayout.Button(isPlaying ? "Stop" : "Play"))
				{
					if (isPlaying)
					{
						script.Stop();
					}
					else
					{
						script.Play();
					}
				}
			}
		}
	}
}
