﻿using System.Collections;
using UnityEngine;

namespace CIMMI
{
	public class AvatarBlinkAnimation : AvatarBlendshapesAnimation
	{
		[SerializeField, Range(0.01f, 1f)]
		private float blinkTime = 0.1f;

		[SerializeField, Range(0.01f, 5f)]
		private float minDelay = 2f, maxDelay = 5f;

		protected override void Awake()
		{
			base.Awake();
			blendShapes = new string[] { "eyeBlinkLeft", "eyeBlinkRight" };
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.K))
			{
				if (IsPlaying)
				{
					Stop();
				}
				else
				{
					Play();
				}
			}
		}

		public override void Play()
		{
			base.Play();
			StartCoroutine(BlinkWaitCoroutine());
		}

		public override void Stop()
		{
			base.Stop();
			IsPlaying = false;
		}

		private IEnumerator BlinkWaitCoroutine()
		{
			while (IsPlaying)
			{
				int left = avatar.sharedMesh.GetBlendShapeIndex(blendShapes[0]);
				int right = avatar.sharedMesh.GetBlendShapeIndex(blendShapes[1]);
				yield return BlinkCoroutine(left, right);

				float wait = Random.Range(minDelay, maxDelay);
				yield return new WaitForSeconds(wait);
			}
		}

		private IEnumerator BlinkCoroutine(int left, int right)
		{
			float time = Time.time;
			bool opening = true;
			while (IsPlaying)
			{
				float t = (Time.time - time) / blinkTime / 2f;
				if (t < 1f)
				{
					float value = opening ? Mathf.SmoothStep(0, 100, t) : Mathf.SmoothStep(100, 0, t);
					avatar.SetBlendShapeWeight(left, value);
					avatar.SetBlendShapeWeight(right, value);
				}
				else
				{
					if (opening)
					{
						time = Time.time;
						opening = false;
					}
					else
					{
						// The animation is over
						yield break;
					}
				}
				yield return null;
			}
		}

		public void SetBlinkTime(float value) => blinkTime = value;
		public void SetMinDelay(float value) => minDelay = value;
		public void SetMaxDelay(float value) => maxDelay = value;
	}
}