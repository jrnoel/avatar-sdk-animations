﻿using UnityEngine;

public abstract class AvatarBlendshapesAnimation : MonoBehaviour
{
	protected string[] blendShapes = null;
	protected SkinnedMeshRenderer avatar;
	protected bool requestStop;

	public bool IsPlaying { get; protected set; }

	protected virtual void Awake()
	{
		avatar = GetComponentInChildren<SkinnedMeshRenderer>();
		if (avatar == null)
		{
			Debug.LogError($"Couldn't find SkinnedMeshRenderer on children of {gameObject.name}");
		}
	}

	public virtual void Play()
	{
		IsPlaying = true;
	}

	public virtual void Stop()
	{
		requestStop = true;
	}
}
